package edu.virginia.cs2110.lab103.team108;

import android.graphics.Bitmap;
import android.graphics.Rect;

/*
 * AmmoBox (Produces ammoboxes for the ghosthunter to pick up. The ammoboxes are placed by
 * moving with the speed of the background. A series of getters and setters are
 * available for getting the position, speed, and existence of the ammobox.)
 */

public class AmmoBox  {
	 
	private int y;
	private int x;
	private int speedX;
	private Background bg;
	public boolean visible = true;
	private Rect box = new Rect(0, 0, 0, 0);
	private Bitmap battery = GameView.getBattery();
	private GameView gv;

	public AmmoBox (int x, int y, GameView gv) {
		this.x = x;
		this.y = y;
		
		this.gv = gv;
		bg = gv.getBg1();
		//box = new Rect(x, y, 79, 87);
		
	}
	
	public Rect getBox() {
		return this.box;
	}

	public double getY() {
		return y;
	}

	public double getX() {
		return x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void update() {
		x += speedX;
		speedX = bg.getSpeedX();
		box.set((int)this.getX(), (int)this.getY(), (int)this.getX() + battery.getWidth(), (int)this.getY() + battery.getHeight());
	}
	
	public void setVisible(boolean b) {
		this.visible = b;
	}
	
	public boolean isVisible() {
		return this.visible;
	}


	
	

}