package edu.virginia.cs2110.lab103.team108;

import android.graphics.Bitmap;
import android.graphics.Rect;

/*
 * Projectile (Updates the view of projectiles by making them visible or invisible. 
 * Also adjusts the speeds of the light bulbs (projectiles) that kill the ghost.)
 */

public class Projectile
{
	private int x, y, speedX;
	private boolean visible;
	private Rect collisionBox = new Rect(0, 0, 0, 0);
	private int width = GameView.getOrb().getWidth();
	private int height = GameView.getOrb().getHeight();
	private GameView gv;

	public Projectile(int startX, int startY, GameView gv)
	{
		x = startX;
		y = startY;
		speedX = (int) (height * 0.2);
		visible = true;
		this.gv = gv;
	}

	/*
	 * update (Changes the location of the light bulb based off of the speed and
	 * adapts when the bulb goes off of the screen)
	 */

	public void update()
	{
		if (GameView.backgroundScrolling)
		{
			x += speedX + gv.getBg1().getSpeedX();
		}
		else
		{
			x += speedX;
		}

		if (x > GameView.convertDP(2000))
		{
			visible = false;
		}

		collisionBox.set(x + (width / 3), y + (height / 3), x
				+ (int) (width * 2.0 / 3.0),
				y + (int) (height * 2.0 / 3.0));
	}

	public Rect getCollisionBox()
	{
		return collisionBox;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getSpeedX()
	{
		return speedX;
	}

	public boolean isVisible()
	{
		return visible;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public void setSpeedX(int speedX)
	{
		this.speedX = speedX;
	}

	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

}
