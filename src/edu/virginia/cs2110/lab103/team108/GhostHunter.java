package edu.virginia.cs2110.lab103.team108;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;

/*
 * GhostHunter (Changes the x and y of the ghosthunter. Also updates the speed and position of the character. 
 * A series of getters and setters for the speed of the character and whether or not the character 
 * has moved are also available. Deals with collisions between the character and ghosts.)
 * 
 */
public class GhostHunter
{
	private Bitmap hunter = GameView.getGhostHunter();

	private int jumpSpeed;
	private int moveSpeed;
	private int topSpeed;
	final int GROUND = GameView.GROUND - GameView.getGhostHunter().getHeight()
			+ GameView.convertDP(20);

	private int centerX;
	private int centerY = GROUND;
	private int limitX;
	private boolean jumped = false;
	private boolean movingLeft = false;
	private boolean movingRight = false;
	private boolean ducked = false;
	private boolean shot = false;
	private boolean dead = false;

	private int speedX = 0;
	private int speedY = 0;
	private int width = GameView.getGhostHunter().getWidth();
	private int score = 0;
	private int highScore = 0;
	private int savedScore = 0;
	private int ammo = 12;
	public static Rect rect = new Rect(0, 0, 0, 0);
	public static Rect rect2 = new Rect(0, 0, 0, 0);
	private static Background bg1, bg2;
	private GameView gv;

	private ArrayList<Projectile> projectiles = new ArrayList<Projectile>();

	public Rect collisionBox = new Rect(0, 0, 0, 0);

	public GhostHunter(GameView gv)
	{
		limitX = gv.getRealWidth() / 6;
		centerX = limitX;
		this.gv = gv;
		bg1 = gv.getBg1();
		bg2 = gv.getBg2();
		this.moveSpeed = (int) (width * 0.04);
		this.jumpSpeed =  (int) -(width * 0.05);
	}

	public int getTopSpeed()
	{
		return topSpeed;
	}

	/*
	 * update (moves the character or scrolls the background accordingly. Also
	 * limits the character from going off of the screen
	 */
	public void update()
	{
		Log.d("GhostHunter", "Updated");
		// Moves Character or Scrolls Background accordingly.
		if (speedX < 0)
		{
			centerX += speedX;
		}
		if (speedX == 0 || speedX < 0)
		{
			bg1.setSpeedX(0);
			bg2.setSpeedX(0);
			GameView.backgroundScrolling = false;
		}
		if (centerX <= limitX && speedX > 0)
		{
			centerX += speedX;
		}
		if (speedX > 0 && centerX > limitX)
		{
			Log.d("GhostHunter", "Background scroll. Movespeed is: "
					+ moveSpeed);
			bg1.setSpeedX(-moveSpeed);
			bg2.setSpeedX(-moveSpeed);
			GameView.backgroundScrolling = true;
		}

		// Updates Y Position
		centerY += speedY;
		if (centerY + speedY >= GROUND)
		{
			centerY = GROUND; // prevents going underground
		}

		// Handles Jumping
		if (jumped == true)
		{
			speedY += 1; // gravity

			if (centerY + speedY >= GROUND) // stops jump on ground
			{
				centerY = GROUND;
				speedY = 0;
				jumped = false;
			}

		}

		// Prevents going beyond X coordinate of 0
		if (centerX + speedX <= 0)
		{
			centerX = 1;
		}

		// // Updates hitboxes
		// rect.setRect(centerX - 56, centerY - 103, 68, 63);
		// rect2.setRect(rect.getX(), rect.getY() + 103, 68, 64);

		collisionBox.set(centerX + (hunter.getWidth() / 5), centerY, centerX
				+ (int) (hunter.getWidth() * (3.5 / 5.0)), centerY
				+ (int) (hunter.getHeight() * (4.0 / 5.0)));
	}

	/*
	 * jump (allows the character to jump)
	 */
	public void jump()
	{
		if (jumped == false)
		{
			speedY = jumpSpeed;
			jumped = true;
		}

	}

	/*
	 * shoot (Adapts the position of the projectile based off where the
	 * flashlight of the hunter is)
	 */
	public void shoot()
	{
		// Change starting position of p based on where the flashlight of the
		// hunter is
		if (!shot && ammo > 0) // prevents multiple shots from a single keypress
		{
			Projectile p = new Projectile(centerX
					+ (int) (hunter.getWidth() * 8.0 / 9.0),
					(centerY + hunter.getHeight() / 2) - GameView.convertDP(15), gv);
			projectiles.add(p);
			this.ammo -= 1;
		}
	}

	/*
	 * moveRight (Unless the sprite is ducked, allows character to move right)
	 */
	public void moveRight()
	{
		setMovingRight(true);
		speedX = moveSpeed;
	}

	/*
	 * moveLeft (Unless the sprite is ducked, allows character to move left)
	 */
	public void moveLeft()
	{
		setMovingLeft(true);
		speedX = -moveSpeed;
	}

	/*
	 * stop (Stops the characters motion so that when no button is being pressed
	 * the character stays still)
	 */
	private void stop()
	{
		if (isMovingRight() == false && isMovingLeft() == false)
		{
			speedX = 0;
		}

		if (isMovingRight() == false && isMovingLeft() == true)
		{
			moveLeft();
		}

		if (isMovingRight() == true && isMovingLeft() == false)
		{
			moveRight();
		}

	}

	public void die()
	{
		dead = true;
	}

	public boolean isDead()
	{
		return dead;
	}

	public void setDead(boolean dead)
	{
		this.dead = dead;
	}

	public Rect getCollisionBox()
	{
		return collisionBox;
	}

	public int getCenterX()
	{
		return centerX;
	}

	public int getCenterY()
	{
		return centerY;
	}

	public boolean isJumped()
	{
		return jumped;
	}

	public boolean isMovingLeft()
	{
		return movingLeft;
	}

	public boolean isMovingRight()
	{
		return movingRight;
	}

	public boolean isDucked()
	{
		return ducked;
	}

	public int getSpeedX()
	{
		return speedX;
	}

	public int getSpeedY()
	{
		return speedY;
	}

	public void setCenterX(int centerX)
	{
		this.centerX = centerX;
	}

	public void setCenterY(int centerY)
	{
		this.centerY = centerY;
	}

	public void setJumped(boolean jumped)
	{
		this.jumped = jumped;
	}

	public void setMovingLeft(boolean movingLeft)
	{
		this.movingLeft = movingLeft;
	}

	public void setMovingRight(boolean movingRight)
	{
		this.movingRight = movingRight;
	}

	public void setDucked(boolean ducked)
	{
		this.ducked = ducked;
	}

	public void setSpeedX(int speedX)
	{
		this.speedX = speedX;
	}

	public void setSpeedY(int speedY)
	{
		this.speedY = speedY;
	}

	public void stopRight()
	{
		setMovingRight(false);
		stop();
	}

	public void stopLeft()
	{
		setMovingLeft(false);
		stop();
	}

	public boolean isShot()
	{
		return shot;
	}

	public void setShot(boolean shot)
	{
		this.shot = shot;
	}

	public ArrayList<Projectile> getProjectiles()
	{
		return projectiles;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}

	public void setHighScore(int highScore, int score)
	{
		if (this.score > score)
		{
			score = highScore;
		}

		else
		{
			highScore = score;
		}
	}

	public int getHighScore()
	{
		return highScore;
	}

	public void pickedUpAmmo()
	{
		this.ammo += 5;

	}

	public int getAmmo()
	{
		return this.ammo;
	}

}
