package edu.virginia.cs2110.lab103.team108;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class GameButton
{
	private int x, y, width, height;
	private Bitmap b;

	public GameButton(int x, int y, Bitmap b)
	{
		this.x = x;
		this.y = y;
		this.b = b;
		if (b != null)
		{
			this.width = b.getWidth();
			this.height = b.getHeight();
		}
		else
		{
			this.width = 0;
			this.height = 0;
		}
	}

	public void draw(Canvas c, Paint p)
	{
		c.drawBitmap(b, x, y, p);
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public Bitmap getB()
	{
		return b;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public void setB(Bitmap b)
	{
		this.b = b;
		this.width = b.getWidth();
		this.height = b.getHeight();
	}

}
