package edu.virginia.cs2110.lab103.team108;

import android.graphics.Bitmap;
import android.graphics.Rect;

/*
 * Ghost (Changes the x and y of the ghost. Also updates the speed and position of the ghost. 
 * A series of getters and setters for the status of the ghost's health as well as whether or not
 * you can see the ghost are available. Deals with collisions between the character and ghosts.)
 */
public class Ghost
{

	private GameView gv;
	private int centerX, centerY, score;
	private int moveSpeed;
	private GhostHunter g;
	private boolean visible = true;
	private boolean dead = false;
	private int width = GameView.getEnemy().getWidth();
	private int height = GameView.getEnemy().getHeight();
	private Rect collisionBox = new Rect(0, 0, 0, 0);
	private Rect collisionBox2 = new Rect(0, 0, 0, 0);

	private Background bg;

	// Behavioral Methods
	public Ghost(int x, int y, GameView gv)
	{
		this.gv = gv;
		bg = gv.getBg1();
		centerX = x;
		centerY = y;
		this.moveSpeed = (int) -(width * .04);
	}

	/*
	 * update (changes the speed of the ghost and deals with what happens when a
	 * collision occurs between the character and the ghost)
	 */
	public void update()
	{
		if (GameView.backgroundScrolling)
		{
			centerX += moveSpeed + bg.getSpeedX();
		}
		else
		{
			centerX += moveSpeed;
		}
		// collisionBox = new Rect(centerX + 25, centerY, 50, 75);
		// collisionBox2 = new Rect((int) collisionBox.getX() + 40,
		// (int) collisionBox.getY() + 65, 50, 50);
		collisionBox.set(centerX + (int) (width * .25), centerY
				+ (int) (height * .10), centerX
				+ (width / 2), centerY
				+ (width / 2));
		collisionBox2.set(centerX + (int) (width * .5), centerY
				+ height / 2, centerX
				+ (int) (width * .8),
				centerY + (int) (height * .9));
	}

	public int getScore()
	{
		score = g.getScore();
		return score;
	}

	public boolean isDead()
	{
		return dead;
	}

	public void setDead(boolean dead)
	{
		this.dead = dead;
	}

	public boolean isVisible()
	{
		return visible;
	}

	public void setVisible(boolean visible)
	{
		this.visible = visible;
	}

	public int getMoveSpeed()
	{
		return moveSpeed;
	}

	public void setMoveSpeed(int moveSpeed)
	{
		this.moveSpeed = moveSpeed;
	}

	public Rect getCollisionBox2()
	{
		return collisionBox2;
	}

	public void setCollisionBox(Rect r)
	{
		collisionBox = r;
	}

	public void setCollisionBox2(Rect r)
	{
		collisionBox2 = r;
	}

	public Rect getCollisionBox()
	{
		return collisionBox;
	}

	public void die()
	{
		dead = true;
	}

	public int getCenterX()
	{
		return centerX;
	}

	public int getCenterY()
	{
		return centerY;
	}

	public Background getBg()
	{
		return bg;
	}

	public void setCenterX(int centerX)
	{
		this.centerX = centerX;
	}

	public void setCenterY(int centerY)
	{
		this.centerY = centerY;
	}

	public void setBg(Background bg)
	{
		this.bg = bg;
	}
}
