package edu.virginia.cs2110.lab103.team108;

import edu.virginia.cs2110.lab103.team108.R;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/*
 * SplashActivity (Shows the initial splash screen when the app is launched)
 */

public class SplashActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		View decorView = getWindow().getDecorView();
		int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
		decorView.setSystemUiVisibility(uiOptions);
		ActionBar actionBar = getActionBar();
		actionBar.hide();

		setContentView(R.layout.activity_splash);

		Thread splashTimer = new Thread()
		{
			public void run()
			{
				try
				{
					sleep(3000);
					Intent intent = new Intent(SplashActivity.this,
							MainActivity.class);
					startActivity(intent);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				finally
				{
					finish();
				}
			}
		};
		splashTimer.start();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		
		if (android.os.Build.VERSION.SDK_INT >= 19)
		{
			if (hasFocus)
			{
				View decorView = getWindow().getDecorView();
				decorView
						.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
								| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
								| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
								| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
								| View.SYSTEM_UI_FLAG_FULLSCREEN
								| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
			}
		}
	}
}
