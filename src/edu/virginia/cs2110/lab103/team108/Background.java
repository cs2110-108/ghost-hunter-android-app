package edu.virginia.cs2110.lab103.team108;

import android.util.Log;

/*
 * Background (Changes the x and y of the background. Also updates the speed and position of the background. 
 * A series of getters and setters are available for getting the position and speed of the background.)
 */
public class Background
{

	private int bgX, bgY, speedX, width;

	public Background(int x, int y, int width)
	{
		bgX = x;
		bgY = y;
		speedX = 0;
		this.width = width;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}

	public void update()
	{
		bgX += speedX;

		if (bgX <= -width)
		{
			bgX = width;
			Log.d("Background", "Switched");
		}
	}

	public int getBgX()
	{
		return bgX;
	}

	public int getBgY()
	{
		return bgY;
	}

	public int getSpeedX()
	{
		return speedX;
	}

	public void setBgX(int bgX)
	{
		this.bgX = bgX;
	}

	public void setBgY(int bgY)
	{
		this.bgY = bgY;
	}

	public void setSpeedX(int speedX)
	{
		this.speedX = speedX;
	}

}
