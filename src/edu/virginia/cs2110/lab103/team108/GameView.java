package edu.virginia.cs2110.lab103.team108;

import java.util.ArrayList;
import java.util.Random;

import edu.virginia.cs2110.lab103.team108.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

/*
 * GameView (Deals with creating the view of the app. Here the screen is painted
 * and a series of commands are dealt with including when the game is running, paused,
 * ended, or terminated. Ghost and ghosthunter movement are also implemented here)
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback
{
	private MainGameActivity mga;
	private GameView gv = this;

	private static final int RUNNING = 1;
	private static final int PAUSED = 2;
	private static final int GAME_OVER = 3;
	private static final int TERMINATED = 4;

	private final int layout = getResources().getConfiguration().screenLayout;
	private float scale = 1.0f;

	public static int GROUND;
	private static MainThread thread;

	private int height = mga.height;
	private int width = mga.width;

	private int scoreThreshold = 100;
	private GhostHunter player;
	private Ghost ghost;
	private Background bg1, bg2;
	private static Bitmap ghostHunter, enemy, background1, background2, orb,
			pause, resume, jump, shoot, play, battery, ground1, ground2, menu;
	private static ArrayList<Bitmap> bitmaps = new ArrayList();
	private GameButton pauseB, jumpB, shootB, resumeB, menuB, playB;

	private int gameState = RUNNING;
	private Object mPauseLock;
	private boolean mPaused;
	public static boolean backgroundScrolling = false;

	private static Context context;

	private ArrayList<Ghost> ghosts = new ArrayList<Ghost>();
	private ArrayList<AmmoBox> batteries = new ArrayList<AmmoBox>();

	private static Random rand;

	private static Paint scorePaint = new Paint();
	Paint batteryPaint = new Paint();
	private static int scoreTextHeight;
	private static int batteryTextHeight;

	private boolean justPaused = false;
	private boolean surfaceRecreated = false;

	public GameView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		mPauseLock = new Object();
		this.context = context;
		getHolder().addCallback(this);
		thread = new MainThread(getHolder(), this);
		setFocusable(true);
		mga = (MainGameActivity) context;

		if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL)
			scale = 0.5f;
		else if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE)
			scale = 1.5f;
		else if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE)
			scale = 2f;
		Log.d("SCALE", Float.toString(scale));

		// BitmapDrawable Setups
		bitmaps.add(ghostHunter = getScaledBitmap(R.drawable.hunter));
		bitmaps.add(background1 = getScaledBitmap(R.drawable.background));
		bitmaps.add(background2 = getScaledBitmap(R.drawable.background));
		bitmaps.add(enemy = getScaledBitmap(R.drawable.ghost1));
		bitmaps.add(orb = getScaledBitmap(R.drawable.orb));
		bitmaps.add(pause = getScaledBitmap(R.drawable.pause_button));
		bitmaps.add(jump = getScaledBitmap(R.drawable.jump_button));
		bitmaps.add(shoot = getScaledBitmap(R.drawable.flashlight));
		bitmaps.add(play = getScaledBitmap(R.drawable.play));
		bitmaps.add(menu = getScaledBitmap(R.drawable.menu));
		bitmaps.add(ground1 = getScaledBitmap(R.drawable.ground));
		bitmaps.add(ground2 = getScaledBitmap(R.drawable.ground));
		bitmaps.add(battery = getScaledBitmap(R.drawable.battery));
		bitmaps.add(resume = getScaledBitmap(R.drawable.resume));

		// Button Setups
		pauseB = new GameButton(0, 0, pause);
		jumpB = new GameButton((int) (convertDP(10) * scale), height
				- jump.getHeight(), jump);
		shootB = new GameButton((int) (width - shoot.getWidth() - convertDP(10)
				* scale), height - shoot.getHeight(), shoot);
		menuB = new GameButton((int) ((width / 2) - (menu.getWidth() * 1.2)),
				(height / 2) + menu.getHeight(), menu);
		playB = new GameButton((int) ((width / 2) + (play.getWidth() * .2)),
				(height / 2) + play.getHeight(), play);
		resumeB = new GameButton(
				(int) ((width / 2) + (resume.getWidth() * .2)), (height / 2)
						+ resume.getHeight(), resume);

		GROUND = height - shoot.getHeight() - convertDP(10);

		// Variable instantiation
		bg1 = new Background(0, 0, background1.getWidth());
		bg2 = new Background(background1.getWidth(), 0, background2.getWidth());
		player = new GhostHunter(this);

		scorePaint.setColor(Color.WHITE);
		scorePaint.setTextSize(90);
		scorePaint.setTextAlign(Align.RIGHT);

		batteryPaint.setTextSize(60);
		batteryPaint.setTextAlign(Align.LEFT);

		Log.d("GAMEVIEW", "Created");
	}

	public GameView(Context context)
	{
		super(context);
		mPauseLock = new Object();
		this.context = context;
		getHolder().addCallback(this);
		thread = new MainThread(getHolder(), this);
		setFocusable(true);
		mga = (MainGameActivity) context;

		if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL)
			scale = 0.5f;
		else if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE)
			scale = 1.5f;
		else if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE)
			scale = 2f;
		Log.d("SCALE", Float.toString(scale));

		// BitmapDrawable Setups
		bitmaps.add(ghostHunter = getScaledBitmap(R.drawable.hunter));
		bitmaps.add(background1 = getScaledBitmap(R.drawable.background));
		bitmaps.add(background2 = getScaledBitmap(R.drawable.background));
		bitmaps.add(enemy = getScaledBitmap(R.drawable.ghost1));
		bitmaps.add(orb = getScaledBitmap(R.drawable.orb));
		bitmaps.add(pause = getScaledBitmap(R.drawable.pause_button));
		bitmaps.add(jump = getScaledBitmap(R.drawable.jump_button));
		bitmaps.add(shoot = getScaledBitmap(R.drawable.flashlight));
		bitmaps.add(play = getScaledBitmap(R.drawable.play));
		bitmaps.add(menu = getScaledBitmap(R.drawable.menu));
		bitmaps.add(ground1 = getScaledBitmap(R.drawable.ground));
		bitmaps.add(ground2 = getScaledBitmap(R.drawable.ground));
		bitmaps.add(battery = getScaledBitmap(R.drawable.battery));
		bitmaps.add(resume = getScaledBitmap(R.drawable.resume));

		// Button Setups
		pauseB = new GameButton(0, 0, pause);
		jumpB = new GameButton((int) (convertDP(10) * scale), height
				- jump.getHeight(), jump);
		shootB = new GameButton((int) (width - shoot.getWidth() - convertDP(10)
				* scale), height - shoot.getHeight(), shoot);
		menuB = new GameButton((int) ((width / 2) - (menu.getWidth() * 1.2)),
				(height / 2) + menu.getHeight(), menu);
		playB = new GameButton((int) ((width / 2) + (play.getWidth() * .2)),
				(height / 2) + play.getHeight(), play);
		resumeB = new GameButton(
				(int) ((width / 2) + (resume.getWidth() * .2)), (height / 2)
						+ resume.getHeight(), resume);

		GROUND = height - shoot.getHeight() - convertDP(10);

		// Variable instantiation
		bg1 = new Background(0, 0, background1.getWidth());
		bg2 = new Background(background1.getWidth(), 0, background2.getWidth());
		player = new GhostHunter(this);

		scorePaint.setColor(Color.WHITE);
		scorePaint.setTextSize(90);
		scorePaint.setTextAlign(Align.RIGHT);

		batteryPaint.setTextSize(60);
		batteryPaint.setTextAlign(Align.LEFT);

		Log.d("GAMEVIEW", "Created");
	}

	public GameView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		mPauseLock = new Object();
		this.context = context;
		getHolder().addCallback(this);
		thread = new MainThread(getHolder(), this);
		setFocusable(true);
		mga = (MainGameActivity) context;

		if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL)
			scale = 0.5f;
		else if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE)
			scale = 1.5f;
		else if ((layout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE)
			scale = 2f;
		Log.d("SCALE", Float.toString(scale));

		// BitmapDrawable Setups
		bitmaps.add(ghostHunter = getScaledBitmap(R.drawable.hunter));
		bitmaps.add(background1 = getScaledBitmap(R.drawable.background));
		bitmaps.add(background2 = getScaledBitmap(R.drawable.background));
		bitmaps.add(enemy = getScaledBitmap(R.drawable.ghost1));
		bitmaps.add(orb = getScaledBitmap(R.drawable.orb));
		bitmaps.add(pause = getScaledBitmap(R.drawable.pause_button));
		bitmaps.add(jump = getScaledBitmap(R.drawable.jump_button));
		bitmaps.add(shoot = getScaledBitmap(R.drawable.flashlight));
		bitmaps.add(play = getScaledBitmap(R.drawable.play));
		bitmaps.add(menu = getScaledBitmap(R.drawable.menu));
		bitmaps.add(ground1 = getScaledBitmap(R.drawable.ground));
		bitmaps.add(ground2 = getScaledBitmap(R.drawable.ground));
		bitmaps.add(battery = getScaledBitmap(R.drawable.battery));
		bitmaps.add(resume = getScaledBitmap(R.drawable.resume));

		// Button Setups
		pauseB = new GameButton(0, 0, pause);
		jumpB = new GameButton((int) (convertDP(10) * scale), height
				- jump.getHeight(), jump);
		shootB = new GameButton((int) (width - shoot.getWidth() - convertDP(10)
				* scale), height - shoot.getHeight(), shoot);
		menuB = new GameButton((int) ((width / 2) - (menu.getWidth() * 1.2)),
				(height / 2) + menu.getHeight(), menu);
		playB = new GameButton((int) ((width / 2) + (play.getWidth() * .2)),
				(height / 2) + play.getHeight(), play);
		resumeB = new GameButton(
				(int) ((width / 2) + (resume.getWidth() * .2)), (height / 2)
						+ resume.getHeight(), resume);

		GROUND = height - shoot.getHeight() - convertDP(10);

		// Variable instantiation
		bg1 = new Background(0, 0, background1.getWidth());
		bg2 = new Background(background1.getWidth(), 0, background2.getWidth());
		player = new GhostHunter(this);

		scorePaint.setColor(Color.WHITE);
		scorePaint.setTextSize(90);
		scorePaint.setTextAlign(Align.RIGHT);

		batteryPaint.setTextSize(60);
		batteryPaint.setTextAlign(Align.LEFT);

		Log.d("GAMEVIEW", "Created");
	}

	public void update()
	{
		// Check ghosts
		// Adds ghosts if less than three exist
		while (ghosts.size() < 3)
		{
			int y = randInt((int) (GROUND - (ghostHunter.getHeight() * 1.25)),
					GROUND - enemy.getHeight());
			// int x = (int) (Math.random() * ((width * 2) - width) + width);
			int x = 0;

			// if (y >= GROUND - (ghostHunter.getHeight() * 1.25))
			// y = (int) (GROUND - (ghostHunter.getHeight() * 1.25));
			// else if (y <= GROUND - enemy.getHeight())
			// y = GROUND - enemy.getHeight();
			if (ghosts.size() == 0)
			{
				x = width;
			}
			else
				x = (int) (ghosts.get(ghosts.size() - 1).getCenterX() + convertDP(250)
						* scale);
			Ghost g = new Ghost(x, y, gv);
			ghosts.add(g);
		}

		// Add batteries
		while (batteries.size() < 3)
		{
			if (batteries.size() == 0)
			{
				AmmoBox g = new AmmoBox(randInt(width, width * 2), 300, gv);
				batteries.add(g);
			}
			else
			{
				AmmoBox g1 = batteries.get(batteries.size() - 1);
				AmmoBox g2 = new AmmoBox((int) g1.getX()
						+ randInt(width * 3, width * 4), 300, gv);
				batteries.add(g2);
			}
		}

		// Update player
		player.update();

		// Clean up projectiles
		ArrayList<Projectile> projectiles = player.getProjectiles();
		for (int i = 0; i < projectiles.size(); i++)
		{
			Projectile proj = (Projectile) projectiles.get(i);

			if (proj.isVisible())
			{
				proj.update();
			}
			else
			{
				projectiles.remove(proj);
			}
		}

		// Update ghost speed
		for (int i = 0; i < ghosts.size(); i++)
		{
			Ghost g = ghosts.get(i);
			int currentSpeed = g.getMoveSpeed();
			if (currentSpeed > scoreThreshold)
			{
				for (Ghost gh : ghosts)
					gh.setMoveSpeed((int) (currentSpeed + (currentSpeed * .05)));
				scoreThreshold *= 2;
			}

			if (g.getCenterX() <= -width)
			{
				ghosts.remove(g);
			}
			if (g.isDead())
				ghosts.remove(g);
			g.update();
		}

		// Clean up batteries
		for (int i = 0; i < batteries.size(); i++)
		{

			AmmoBox g = batteries.get(i);
			if (g.getX() <= convertDP(-110))
			{
				g.setVisible(false);
			}
			if (!g.isVisible())
				batteries.remove(g);
			else
				g.update();
		}

		bg1.update();
		bg2.update();
		checkCollisions();
	}

	public boolean render(Canvas c, Paint p)
	{
		if (c != null)
		{
			c.drawColor(0, Mode.CLEAR);

			// Draw images
			c.drawBitmap(background1, bg1.getBgX(), bg1.getBgY(), p);
			c.drawBitmap(background2, bg2.getBgX(), bg2.getBgY(), p);
			c.drawBitmap(ground1, bg1.getBgX(), GROUND, p);
			c.drawBitmap(ground2, bg2.getBgX(), GROUND, p);

			Log.d("BACKGROUND1", Integer.toString(bg1.getBgX()));
			Log.d("BACKGROUND2", Integer.toString(bg2.getBgX()));

			// Draw Batteries
			for (AmmoBox b : batteries)
			{
				c.drawBitmap(getBattery(), (float) b.getX(), (float) b.getY(),
						p);
			}

			// Draw Projectiles
			ArrayList<Projectile> projectiles = player.getProjectiles();
			for (int i = 0; i < projectiles.size(); i++)
			{
				Projectile proj = (Projectile) projectiles.get(i);
				c.drawBitmap(getOrb(), proj.getX(), proj.getY(), p);
			}

			// Draw player
			c.drawBitmap(getGhostHunter(), player.getCenterX(),
					player.getCenterY(), p);

			// Draw Ghosts
			for (Ghost gh : ghosts)
				c.drawBitmap(getEnemy(), gh.getCenterX(), gh.getCenterY(), p);

			// Buttons
			pauseB.draw(c, p);
			jumpB.draw(c, p);
			shootB.draw(c, p);

			// Score
			c.drawText("Score: " + Integer.toString(player.getScore()),
					getWidth() - convertDP(30) * scale,
					(scoreTextHeight * 1.1f), scorePaint);

			// Remaining Battery Life
			if (player.getAmmo() <= 3)
				batteryPaint.setColor(Color.RED);
			else
				batteryPaint.setColor(Color.WHITE);
			c.drawText("Battery Left: " + Integer.toString(player.getAmmo()),
					pauseB.getWidth(), (batteryTextHeight * 1.1f), batteryPaint);

			// Collision Boxes
			// Paint rectPaint = new Paint();
			// rectPaint.setStrokeWidth(3);
			// rectPaint.setColor(Color.RED);
			// for (AmmoBox battery : batteries)
			// {
			// c.drawRect(battery.getBox(), rectPaint);
			// }
			// for (Ghost gh : ghosts)
			// {
			// c.drawRect(gh.getCollisionBox(), rectPaint);
			// c.drawRect(gh.getCollisionBox2(), rectPaint);
			// }
			// c.drawRect(player.getCollisionBox(), rectPaint);
			// for (Projectile proj : projectiles)
			// {
			// c.drawRect(proj.getCollisionBox(), rectPaint);
			// }
			return true;
		}
		else
			return false;
	}

	public int getRealHeight()
	{
		return height;
	}

	public int getRealWidth()
	{
		return width;
	}

	/**
	 * Returns a pseudo-random number between min and max, inclusive. The
	 * difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min
	 *            Minimum value
	 * @param max
	 *            Maximum value. Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randInt(int min, int max)
	{

		rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}

	public static Bitmap getPause()
	{
		return pause;
	}

	public static Bitmap getResume()
	{
		return resume;
	}

	public static Bitmap getJump()
	{
		return jump;
	}

	public static Bitmap getShoot()
	{
		return shoot;
	}

	public static Bitmap getQuit()
	{
		return menu;
	}

	public static Bitmap getPlay()
	{
		return play;
	}

	public Bitmap getBackground1()
	{
		return background1;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		Log.d("GAMEVIEW", "SurfaceCreated Called");

		Rect bounds = new Rect();
		scorePaint.getTextBounds("S", 0, 1, bounds);
		scoreTextHeight = bounds.height();

		Rect bounds2 = new Rect();
		batteryPaint.getTextBounds("B", 0, 1, bounds2);
		batteryTextHeight = bounds.height();

		if (!thread.isAlive())
		{
			thread = new MainThread(getHolder(), this);
			thread.start();
			thread.setRunning(true);
		}
		else
		{
			surfaceRecreated = true;
			synchronized (mPauseLock)
			{
				mPaused = false;
				mPauseLock.notifyAll();
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height)
	{
		this.width = width;
		this.height = height;
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		Log.d("GAMEVIEW", "Surface destroyed");
	}

	public void onPause()
	{
		Log.d("GAMEVIEW", "onPause");
		// if (gameState == RUNNING)
		// gameState = PAUSED;
		// synchronized (mPauseLock)
		// {
		// mPaused = true;
		// }
		if (gameState == RUNNING)
		{
			gameState = PAUSED;
			justPaused = true;
		}

		synchronized (mPauseLock)
		{
			mPaused = true;
		}

	}

	public void onResume()
	{
		Log.d("GAMEVIEW", "onResume");
		// synchronized (mPauseLock)
		// {
		// mPaused = false;
		// mPauseLock.notifyAll();
		// }
	}

	public Bitmap getScaledBitmap(int id)
	{
		Bitmap b = BitmapFactory.decodeResource(getResources(), id);
		int width = b.getWidth();
		int height = b.getHeight();

		width *= scale;
		height *= scale;

		Bitmap resized = Bitmap.createScaledBitmap(b, width, height, false);
		// BitmapDrawable bDrawable = new BitmapDrawable(getResources(),
		// resized);
		return resized;
	}

	public float getScale()
	{
		return scale;
	}

	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		if (getGameState() == RUNNING)
		{
			switch (e.getActionMasked())
			{
			case MotionEvent.ACTION_DOWN:
				float x = e.getX();
				float y = e.getY();
				if (x >= jumpB.getX() && x < jumpB.getX() + jumpB.getWidth()
						&& y >= jumpB.getY()
						&& y < jumpB.getY() + jumpB.getHeight()) // Jump
				// Button
				{
					player.jump();
				}
				else if (x >= shootB.getX() && x < shootB.getX() + getWidth()
						&& y >= shootB.getY()
						&& y < shootB.getY() + shootB.getHeight()) // Shoot
																	// Button
				{
					player.shoot();
				}
				else if (x >= pauseB.getX()
						&& x < pauseB.getX() + pauseB.getWidth()
						&& y >= pauseB.getY()
						&& y <= pauseB.getY() + pauseB.getHeight()) // Pause
																	// button
				{
					gameState = PAUSED;
				}
				else if (x >= width / 2) // right side of screen
				{
					player.moveRight();
					player.setMovingRight(true);
					Log.d("TouchEvent", "moved right");
				}
				else if (x < width / 2) // left side of screen
				{
					player.moveLeft();
					player.setMovingLeft(true);
				}
				break;
			case MotionEvent.ACTION_UP:
				if (player.isMovingRight())
				{
					player.stopRight();
					player.setMovingRight(false);
				}
				else
				{
					player.stopLeft();
					player.setMovingLeft(false);
				}
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				float x1 = e.getX(e.getActionIndex());
				float y2 = e.getY(e.getActionIndex());
				if (x1 >= jumpB.getX() && x1 < jumpB.getX() + jumpB.getWidth()
						&& y2 >= jumpB.getY()
						&& y2 < jumpB.getY() + jumpB.getHeight()) // Jump
				// Button
				{
					if (!player.isJumped())
					{
						player.jump();
					}
				}
				else if (x1 >= shootB.getX() && x1 < shootB.getX() + getWidth()
						&& y2 >= shootB.getY()
						&& y2 < shootB.getY() + shootB.getHeight()) // Shoot
				// Button
				{
					if (!player.isShot())
						player.shoot();
				}
				else if (x1 > getWidth() / 2)
				{
					if (!player.isMovingRight() && !player.isMovingLeft())
					{
						player.moveRight();
					}
				}
				else
				{
					if (!player.isMovingLeft() && !player.isMovingRight())
					{
						player.moveLeft();
					}
				}
				break;
			case MotionEvent.ACTION_POINTER_UP:

				break;
			}
		}
		else if (getGameState() == PAUSED)
		{
			float x = e.getX();
			float y = e.getY();
			switch (e.getAction())
			{
			case MotionEvent.ACTION_DOWN:
				if (x >= pauseB.getX() && x < pauseB.getX() + pauseB.getWidth()
						&& y >= pauseB.getY()
						&& y <= pauseB.getY() + pauseB.getHeight()) // Pause
																	// button
				{
					gameState = RUNNING;
				}
				else if (x >= menuB.getX()
						&& x < menuB.getX() + menuB.getWidth()
						&& y >= menuB.getY()
						&& y < menuB.getY() + menuB.getHeight()) // Menu
				// Button
				{
					if (context instanceof MainGameActivity)
					{
						setGameState(TERMINATED);
						MainGameActivity a = (MainGameActivity) context;
						a.mainMenu();
					}
				}
				else if (x >= resumeB.getX()
						&& x < resumeB.getX() + resumeB.getWidth()
						&& y >= resumeB.getY()
						&& y < resumeB.getY() + resumeB.getHeight()) // Resume
																		// button
				{
					gameState = RUNNING;
				}
				break;
			}
		}
		else
		// Game is over
		{
			float x = e.getX();
			float y = e.getY();

			switch (e.getAction())
			{
			case MotionEvent.ACTION_DOWN:
				if (x >= playB.getX() && x < playB.getX() + playB.getWidth()
						&& y >= playB.getY()
						&& y < playB.getY() + playB.getHeight()) // Play
				// again
				// button
				{
					if (context instanceof MainGameActivity)
					{
						gameState = TERMINATED;
						mga.playAgain();
					}
				}
				else if (x >= menuB.getX()
						&& x < menuB.getX() + menuB.getWidth()
						&& y >= menuB.getY()
						&& y < menuB.getY() + menuB.getHeight()) // Menu
				// button
				{
					if (context instanceof MainGameActivity)
					{
						gameState = TERMINATED;
						mga.mainMenu();
					}
				}
				break;
			}
		}
		return true;

	}

	public static int getRunning()
	{
		return RUNNING;
	}

	public static int getPaused()
	{
		return PAUSED;
	}

	public static int getGameOver()
	{
		return GAME_OVER;
	}

	public static int getTerminated()
	{
		return TERMINATED;
	}

	@Override
	protected void onDraw(Canvas c)
	{
		super.onDraw(c);
	}

	/*
	 * checkCollisions (Deals with collisions between ghost hunters and ghosts
	 * as well as between the projectile and ghosts).
	 */
	public void checkCollisions()
	{
		for (Ghost ghost : ghosts)
		{
			if (player.getCollisionBox().intersect(ghost.getCollisionBox())
					|| player.getCollisionBox().intersect(
							ghost.getCollisionBox2()))
			{
				player.die();
				ghost.setVisible(false);
				setGameState(GAME_OVER);
				Log.d("GAMEOVER", "Game is over");
			}
			for (Projectile p : player.getProjectiles())
			{
				if (ghost.getCollisionBox().intersect(p.getCollisionBox())
						|| ghost.getCollisionBox2().intersect(
								p.getCollisionBox()))
				{
					ghost.die();
					player.setScore(player.getScore() + 10);
					p.setVisible(false);
					int x = (int) (Math.random() * 8 + 1);
					if (x == 2)
					{
						AmmoBox b = new AmmoBox(ghost.getCenterX(),
								ghost.getCenterY(), this);
						batteries.add(b);
					}
				}
			}
		}

		for (AmmoBox b : batteries)
		{

			if (player.getCollisionBox().intersect(b.getBox()))
			{
				player.pickedUpAmmo();
				b.setVisible(false);

			}
		}
	}

	public static int convertDP(float dp)
	{
		Resources resources = context.getResources();
		float density = resources.getDisplayMetrics().density;
		int p = (int) (dp * density + 0.5f);
		return p;
	}

	// public void proximityAlert()
	// {
	// Canvas c1 = getHolder().lockCanvas();
	// Paint p1 = new Paint();
	// c1.save();
	// while (player.getCenterX() - ghost.getCenterX() <= 5)
	// {
	// p1.setTypeface(Typeface.DEFAULT_BOLD);
	// p1.setTextSize(50);
	// c1.drawText("Danger", 250, 250, p1);
	// c1.drawBitmap(menuB, 250, 250, p1);
	// getHolder().unlockCanvasAndPost(c1);
	// }
	// c1.restore();
	// }

	public int getGROUND()
	{
		return GROUND;
	}

	public GhostHunter getPlayer()
	{
		return player;
	}

	public Background getBg1()
	{
		return bg1;
	}

	public Background getBg2()
	{
		return bg2;
	}

	public static Bitmap getBattery()
	{
		return battery;
	}

	public static void setBattery(Bitmap battery)
	{
		GameView.battery = battery;
	}

	public static Bitmap getGhostHunter()
	{
		return ghostHunter;
	}

	public static Bitmap getEnemy()
	{
		return enemy;
	}

	public static Bitmap getOrb()
	{
		return orb;
	}

	public int getGameState()
	{
		return gameState;
	}

	public void setGameState(int gameState)
	{
		this.gameState = gameState;
	}

	public void recycleBitmaps()
	{
		if (gameState != PAUSED)
		{
			ghostHunter.recycle();
			ghostHunter = null;
			enemy.recycle();
			enemy = null;
			background1.recycle();
			background1 = null;
			background2.recycle();
			background2 = null;
			orb.recycle();
			orb = null;
			pause.recycle();
			pause = null;
			resume.recycle();
			resume = null;
			jump.recycle();
			jump = null;
			shoot.recycle();
			shoot = null;
			play.recycle();
			play = null;
			battery.recycle();
			battery = null;
			ground1.recycle();
			ground1 = null;
			ground2.recycle();
			ground2 = null;
			menu.recycle();
			menu = null;

			System.gc();
		}
	}

	public class MainThread extends Thread
	{
		private final String TAG = MainThread.class.getSimpleName();

		private boolean running;
		private boolean pauseRender = true;
		private boolean wait;
		private Paint renderPaint = new Paint();
		private int run = 1;

		public MainThread(SurfaceHolder surfaceHolder, GameView gameView)
		{
			super();
		}

		public void setRunning(boolean running)
		{
			this.running = running;
		}

		@Override
		public void run()
		{
			while (getGameState() == RUNNING || getGameState() == PAUSED)
			{
				if (getGameState() == RUNNING)
				{
					pauseRender = true;
					update();
					Canvas c = getHolder().lockCanvas();
					render(c, renderPaint);
					getHolder().unlockCanvasAndPost(c);
				}
				else if (getGameState() == PAUSED)
				{
						Paint p2 = new Paint();
						p2.setTypeface(Typeface.DEFAULT_BOLD);
						p2.setTextSize(200);
						p2.setTextAlign(Align.CENTER);
						p2.setColor(Color.BLUE);
						
						Canvas c2 = getHolder().lockCanvas();
						while (!render(c2, renderPaint));
						Log.d("GAMEVIEW", "rendered");
						c2.drawText("PAUSED", (width / 2), (height / 2), p2);
						menuB.draw(c2, p2);
						resumeB.draw(c2, p2);
						getHolder().unlockCanvasAndPost(c2);
				}
				synchronized (mPauseLock)
				{
					while (mPaused)
					{
						try
						{
							mPauseLock.wait();
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
				}
			}

			if (getGameState() == GAME_OVER)
			{
				Canvas c2 = getHolder().lockCanvas();
				Paint p2 = new Paint();
				p2.setTypeface(Typeface.DEFAULT_BOLD);
				p2.setTextSize(200);
				p2.setTextAlign(Align.CENTER);
				p2.setColor(Color.RED);
				while (!render(c2, renderPaint));
				c2.drawText("GAME OVER", (width / 2), (height / 2), p2);
				playB.draw(c2, p2);
				menuB.draw(c2, p2);
				getHolder().unlockCanvasAndPost(c2);
			}
		}
	}
}
