package edu.virginia.cs2110.lab103.team108;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

/*
 * MainGameActivity(Deals with the buttons that are pressed while playing the game 
 * like pause and resume.)
 */
public class MainGameActivity extends Activity
{

	MediaPlayer gameMusic;
	private GameView gv;
	private TextView scoreV, batteryV;
	static int height, width;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		View decorView = getWindow().getDecorView();
	    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
	    decorView.setSystemUiVisibility(uiOptions);
	    ActionBar actionBar = getActionBar();
	    actionBar.hide();
		
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		if (android.os.Build.VERSION.SDK_INT >= 19)
		{
			display.getRealSize(size);
		}
		else
			display.getSize(size);

		width = size.x;
		height = size.y;
		
		setContentView(R.layout.activity_main_game);
		
		gv = (GameView) findViewById(R.id.gameView2);

		Log.d("MGA", "Created");
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		gv.recycleBitmaps();
		unbindDrawables(findViewById(R.id.gameView));
		System.gc();
		Log.d("MGA", "Destroyed");
	}

	private void unbindDrawables(View view)
	{
		if (view.getBackground() != null)
		{
			view.getBackground().setCallback(null);
		}
		if (view instanceof ViewGroup)
		{
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++)
			{
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			((ViewGroup) view).removeAllViews();
		}
	}

	public void mainMenu()
	{
		finish();
	}

	public void playAgain()
	{
		recreate();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		gv.onResume();
		
		gameMusic = MediaPlayer.create(MainGameActivity.this, R.raw.sweatshirt);
		gameMusic.start();
		Log.d("MGA", "Resumed");
	}

	@Override
	protected void onPause()
	{
		gameMusic.release();
		gv.onPause();
		super.onPause();
		
		Log.d("MGA", "Paused");
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		if (android.os.Build.VERSION.SDK_INT >= 19)
		{
			if (hasFocus)
			{
				View decorView = getWindow().getDecorView();
				decorView
						.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
								| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
								| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
								| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
								| View.SYSTEM_UI_FLAG_FULLSCREEN
								| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings)
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
